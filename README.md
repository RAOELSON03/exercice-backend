# exercice-backend-springboot

## What's inside 
This project is based on the [Spring Boot](http://projects.spring.io/spring-boot/) project and uses these packages :
- Maven
- Spring Core
- Spring Data (JPA & H2)
- Spring MVC (Tomcat)
- Spring Security
- JWT


## Installation 
The project is created with Maven, so you just need to import it to your IDE and build the project to resolve the dependencies


## Usage 
Run the project through the IDE and head out to [http://localhost:8080](http://localhost:8080)

or 

run this command in the command line:
```
mvn spring-boot:run
```