package com.exercice.demo;

import java.util.Random;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.exercice.demo.dao.CategorieRepository;
import com.exercice.demo.dao.ProduitRepository;
import com.exercice.demo.dao.UserRepository;
import com.exercice.demo.model.Categorie;
import com.exercice.demo.service.UserService;
import com.exercice.demo.model.Produit;
import com.exercice.demo.model.UserRole;

import net.bytebuddy.utility.RandomString;

@SpringBootApplication
public class ExerciceApplication implements CommandLineRunner{
	
	@Autowired
    private ProduitRepository produitRepository;
	
	@Autowired
    private UserRepository userRepository;
	
    @Autowired
    private CategorieRepository categorieRepository;
    @Autowired
    private RepositoryRestConfiguration repositoryRestConfiguration;
    
    @Autowired
    private UserService userService;

	public static void main(String[] args) {
		SpringApplication.run(ExerciceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		repositoryRestConfiguration.exposeIdsFor(Produit.class,Categorie.class);

		categorieRepository.save(new Categorie("Bagages",null,null,null));
		categorieRepository.save(new Categorie("Bijoux",null,null,null));
		categorieRepository.save(new Categorie("Chaussures et sacs",null,null,null));
		categorieRepository.save(new Categorie("Auto et moto",null,null,null));
        Random rnd=new Random();
        String description = "Ideoque fertur neminem aliquando ob"
        		+ "aetatis progressu effervescebat, obstinatum eius propositum accendente adulatorum cohorte.";
        		
        categorieRepository.findAll().forEach(c->{
            for (int i = 0; i <10 ; i++) {
                Produit p=new Produit();
                p.setName(RandomString.make(18));
                p.setPrix(100+rnd.nextInt(10000));
                p.setDescription(description);
                p.setCategorie(c);
                p.setPhoto(i+".png");
                produitRepository.save(p);
            }
        });
        
        userService.save(new UserRole(null,"USER"));
        userService.save(new UserRole(null,"ADMIN"));
        Stream.of("user","user1","user2","admin").forEach(un->{
        	userService.saveUser(un,"1234","1234");
        });
        userService.addRoleToUser("user","USER");
        
        userRepository.findAll().forEach(users -> {
        	System.out.println(users.getUsername());
        });
	}
	
	@Bean
    BCryptPasswordEncoder getBCPE(){
        return new BCryptPasswordEncoder();
    }

}
