package com.exercice.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exercice.demo.model.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
	 public UserRole findByRoleName(String roleName);
}
