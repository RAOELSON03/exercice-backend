package com.exercice.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.exercice.demo.model.Users;

@CrossOrigin("*")
@RepositoryRestResource
public interface UserRepository extends JpaRepository<Users, Long> {
	 public Users findByUsername(String username);
}
