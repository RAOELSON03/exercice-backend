package com.exercice.demo.service;

import com.exercice.demo.model.UserRole;
import com.exercice.demo.model.Users;

public interface UserService {
	public Users saveUser(String username,String password,String confirmedPassword);
    public UserRole save(UserRole role);
    public Users loadUserByUsername(String username);
    public void addRoleToUser(String username,String rolename);
}
