package com.exercice.demo.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.exercice.demo.dao.UserRepository;
import com.exercice.demo.dao.UserRoleRepository;
import com.exercice.demo.model.Users;
import com.exercice.demo.model.UserRole;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	private UserRoleRepository userRoleRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public UserServiceImpl(UserRepository userRepository, UserRoleRepository userRoleRepository,
			BCryptPasswordEncoder bCryptPasswordEncoder) {
		super();
		this.userRepository = userRepository;
		this.userRoleRepository = userRoleRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	public Users saveUser(String username, String password, String confirmedPassword) {
		Users user = userRepository.findByUsername(username);
		if (user != null)
			throw new RuntimeException("User already exists");
		if (!password.equals(confirmedPassword))
			throw new RuntimeException("Please confirm your password");
		Users appUser = new Users();
		appUser.setUsername(username);
		appUser.setPassword(bCryptPasswordEncoder.encode(password));
		userRepository.save(appUser);
		addRoleToUser(username, "USER");
		return appUser;
	}

	@Override
	public UserRole save(UserRole role) {
		return userRoleRepository.save(role);
	}

	@Override
	public Users loadUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public void addRoleToUser(String username, String rolename) {
		Users user = userRepository.findByUsername(username);
		UserRole userRole = userRoleRepository.findByRoleName(rolename);
		user.getRoles().add(userRole);

	}

}
